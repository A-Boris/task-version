import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    listItems: [],
    currentTasks: [],
    completedTasks: [],
  },
  getters: {
    listItems(state) {
      return state.listItems
    },
    currentTasks(state) {
      return state.currentTasks
    },
    completedTasks(state) {
      return state.completedTasks
    },
  },
  mutations: {
    setListItems(state, payload) {
      state.listItems = payload
    },
    pushListItems(state, payload) {
      state.listItems.push(payload)
    },
    setTasks(state, payload) {
      state.currentTasks = payload.current
      state.completedTasks = payload.completed
    },
  },

  actions: {
    async getAll({ commit }) {
      try {
        const { data } = await axios.get('/test')
        commit('setListItems', data)
      } catch (err) {
        console.log(err.message)
      }
    },
    async createTask({ commit }, payload) {
      try {
        const { data } = await axios.get('/test?value=' + payload.route)

        let template = data[0]
        template.currentTasks.push(payload.text)

        setChange(commit, data, template)
      
      } catch (err) {
        console.log(err.message)
      }
    },

    async createListItems({ commit }, payload) {
      try {
        const { data } = await axios.post('/test', {
          value: payload,
          currentTasks: [],
          completedTasks: [],
        })
        commit('pushListItems', data)
      } catch (err) {
        console.log(err.message)
      }
    },

    async getTasks({ commit }, route) {
      try {
        const { data } = await axios.get('/test?value=' + route)
        commit('setTasks', {
          current: data[0].currentTasks,
          completed: data[0].completedTasks,
        })
      } catch (err) {
        console.log(err.message)
      }
    },

    async editTask({ commit }, payload) {
      try {
        const { data } = await axios.get('/test?value=' + payload.route)
        let template = data[0]
        template.currentTasks[payload.index] = payload.text

        setChange(commit, data, template)

      } catch (err) {
        console.log(err.message)
      }
    },

    async checkTask({ commit }, payload) {
      try {
        const { data } = await axios.get('/test?value=' + payload.route)
        console.log(data);
        let template = data[0]
        let test = template.currentTasks[payload.index]
        template.completedTasks.push(test)
        template.currentTasks.splice(payload.index, 1)

        setChange(commit, data, template)

      } catch (err) {
        console.log(err.message)
      }
    },

    async deleteTask({ commit }, payload) {
      try {
        const { data } = await axios.get('/test?value=' + payload.route)

        let filteredTasks = data[0].currentTasks.filter(
          (item) => item !== payload.text
        )
        let template = data[0]
        template.currentTasks = filteredTasks

        setChange(commit,data, template)
    
      } catch (err) {
        console.log(err.message)
      }
    },
  },
})

async function setChange(commit, data, template) {
  const res = await axios.put('/test/' + data[0].id, { ...template })

  commit('setTasks', {
    current: res.data.currentTasks,
    completed: res.data.completedTasks,
  })
}
